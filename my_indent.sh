#!/bin/sh

mkdir /tmp/indent
rm /tmp/indent/* -r
mkdir /tmp/indent/befor
mkdir /tmp/indent/after
cp $@ /tmp/indent/after

indent -bad -bap -nbc -bbo -hnl -br -ce -brs -ci8 -cli0 -d0 -di1 -nfca -i8 -ip0 -l80 -lp -npcs -nprs -npsl -sai -saf -saw -ncs -sob -nfca -ss -ts8 -il0 /tmp/indent/after/*

cp /tmp/indent/after/*~ /tmp/indent/befor/
rm /tmp/indent/after/*~
cat /tmp/indent/befor/* > /tmp/indent/file2
cat /tmp/indent/after/* > /tmp/indent/file1

diff /tmp/indent/file2 /tmp/indent/file1
